using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Generate random platform sections, there are 3 different sections to choose.
 * 
 * @author: Hardison.W
 */

public class GenPlatform : MonoBehaviour
{
    // Variables
    public GameObject[] sections;
    public int zPos = 50;
    public bool createSec = false;
    public int secIndex;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (createSec == false)
        {
            createSec = true;
            StartCoroutine(GenerateSection());
        }
    }

    // A coroutine to generate random sections
    IEnumerator GenerateSection()
    {
        secIndex = Random.Range(0, 3);
        Instantiate(sections[secIndex], new Vector3(0, 0,zPos), Quaternion.identity);
        zPos += 50;
        yield return new WaitForSeconds(2);
        createSec = false;
    }
}
