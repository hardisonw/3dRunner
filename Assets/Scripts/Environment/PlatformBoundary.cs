using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Environment settings.
 * 
 * @author: Hardison.W
 * 
 */

public class PlatformBoundary : MonoBehaviour
{
    // Variables
    public static float leftBound = -3.5f;
    public static float rightBound = 3.5f;
    public float internalLeft;
    public float internalRight;

    public static 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        internalLeft = leftBound;
        internalRight = rightBound;
    }
}
