using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Basic player movement script.
 * 
 * @author Hardison.W
 * 
 */

public class PlayerMove : MonoBehaviour
{
    // Variables
    public float moveSpeed = 3f;
    public float leftRightSpeed = 4f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.World);
        leftRightMove();
    }

    // Horizontal movenent
    void leftRightMove()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            if (this.gameObject.transform.position.x > PlatformBoundary.leftBound)
            {
                transform.Translate(Vector3.left * Time.deltaTime * leftRightSpeed);
            }
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            if (this.gameObject.transform.position.x < PlatformBoundary.rightBound)
            {
                transform.Translate(Vector3.right * Time.deltaTime * leftRightSpeed);
            }
        }
    }
}
